---
layout: markdown_page
title: "Professional Services"
---
# Welcome to the Professional Services Handbook
{:.no_toc}

The Professional Services team at GitLab is a part of [Customer Success](/handbook/customer-success). On our Professional Servies team, **Implementation Specialists** have the goal to optimize organization's adoption of GitLab through our implementation services, designed to enable other necessary systems in your environment so that your teams can move code from idea to production.

## On this page
{:.no_toc}

- TOC
{:toc}

## Responsibilities

* Install & configure Gitlab solutions in customer environment as per Statement of Work (SOW)
* Provide technical training sessions remotely and on-site
* Develop and implement migration plan for customer VCS & data migration
* Assist Gitlab customer support team to diagnose and troubleshoot support cases when necessary
* Develop & maintain custom scripts for integration or policy to align with custom requirements
* Create detailed documentation for implementation, guides, and training.
* Support Sales on quoting PS and drafting SOW to respond PS requests
* Managing creation of new and maintaining of existing training content


### Statement of Work creation
The GitLab Professional Services team is responsible for both maintaining the [GitLab SOW Template](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit) (internal link) as well as the production of new Statements of Work for customer proposals.

To obtain a Statement of Work, open a new SOW issue using the template on the [Professional Services project](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) (internal link).

### Services Delivery
#### Implementation Plan
Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the Professional Services Team has with the customer.  The Professional Services Team also maintains the SOW template located [at this internal link](https://docs.google.com/document/d/1ohZtqR1mMZYWAOD7PBVc1xZIMGpzHNgccRxVZ9Nzirg/edit).

## Professionl Services Offerings
### Training & Education
Below are the main training courses that we currently offer.  Below find a description of each and an (internal) link to the slides that go along with the training.

* [GitLab with Git Basics](https://docs.google.com/presentation/d/1R_G9YPM3aS-3lUdAm4hu9jfP7xLYjqm5zzgh7AYPyw8/edit): This course is for users that are new to Git and GitLab.  The course walks through the basics of Git (commit, branching, trees, etc.) as well as how those basics work in the context of both the GitLab user interface and interacting with GitLab through the CLI.
* [GitLab Admin Training](https://docs.google.com/presentation/d/1EKOEcQ_8qE0rQADmbzBKVoeMm7eEivkb_S_2rq4TCAU/edit): This course is for new GitLab administrators.  The course covers topics about installing, configuring and maintaining a GitLab instance.
* [GitLab CI/CD Training](https://docs.google.com/presentation/d/1kx9P3n5AOKSXM9x1TWMOsjm7wVfrn12bZOLlJsYkvxo/edit): This course is useful to developers, administrators and DevOps professionals alike.  Topics covered include using GitLab CI/CD with your projects, the .gitlab-ci.yml file and the various ways GitLab can be used as a Complete DevOps platform for the entire SDLC.

### Migration Services
As part of services engagement, we offer migration services from a customer's current systems.  These services include consulting with the customer's technical staff about the best way to migrate not only the customer's source code but also their entire SDLC to the GitLab platform.  We may implement the migration in a phased approach or a replacement approach depending on the customer's comfort level and desires around adoption.

This can include:
* Importing your projects from GitHub, Bitbucket, GitLab.com, FogBugz and SVN into GitLab
* Migrating from SVN: Convert a SVN repository to Git and GitLab

In all cases, a GitLab Adoption Plan will be created with the customer to outline the goals of the migration and adoption, as well as any plans that involve customer or GitLab Professional Services actions.

### Integration Services
The GitLab Professional Services team will also work with a customer's technical team to come up with an Integration Plan for the GitLab implementation.  Typically part of the overall Implementation Plan, this Integration Plan will consider a customer's existing systems and corporate policies in regards to:

* Integration with LDAP/AD or other OAuth services for authentication
*Integration to Jira, Jenkins, Redmine, Mattermost
* Custom Integrations
   - Automation
   - API: Automate GitLab via a simple and powerful API.
   - GitLab Webhooks: Let GitLab notify another system when new code is pushed

### Specialized Trainings
#### Train the Trainer
The GitLab Train the Trainer program is a set of workshops designed to enable super-users at customer organizations to be able to train their team on Git and GitLab best practices.

#### GitLab Certification Program
The Professional Services team will be rolling out a GitLab Certification Program in the second quarter of 2018.  To view the progress of this initiative, see [this internal issue](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/46)

